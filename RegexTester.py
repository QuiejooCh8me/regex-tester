#!/usr/bin/env python3
"""Application."""

import re

import npyscreen

import regex

from StdForms import SplitExitForm
from StdWidgets import FixedMultiLineEdit, FixedRoundCheckBox


class MainForm(SplitExitForm):
    """Main form."""
    # Repurpose the 'OK' button as an 'Exit' button.
    OK_BUTTON_TEXT = 'Exit'

    def create(self):
        """Add widgets to the form."""
        # Top half of the screen (user text entry):)
        # Calculate how much space to give text entry widets.
        halfHeight = self.get_half_way()
        maxEntryHeight = int(halfHeight / 2)
        regexOptY = 2 + (maxEntryHeight - 6)
        # Give the user a place to enter the regex pattern they are testing.
        self.regexPattern = self.add(FixedMultiLineEdit, name='Regex Pattern to Test', max_height=maxEntryHeight - 6, rely=2)
        # Give the user the regex options they can select.
        self.ignoreCase = self.add(FixedRoundCheckBox, name="Ignore Case", relx=2, rely=regexOptY, max_width=19)
        self.multiLine = self.add(FixedRoundCheckBox, name="Multi Line", relx=21, rely=regexOptY, max_width=18)
        self.dotAll = self.add(FixedRoundCheckBox, name="Dot Matches All", relx=39, rely=regexOptY)
        # Give the user a place to enter the string they would like to search.
        self.searchString = self.add(FixedMultiLineEdit, name='String to Search', rely=int(halfHeight / 2) - 2, max_height=maxEntryHeight + 2)
        # Bottom half of the screen (results):
        # Show the full match (if any)
        self.fullMatch = self.add(FixedMultiLineEdit, name='Full Match', editable=False, rely=halfHeight + 2, max_height=maxEntryHeight - 2)
        # Show the matched groups (if any)
        self.groupMatches = self.add(FixedMultiLineEdit, name="Group Matches", editable=False, rely=halfHeight + maxEntryHeight, max_height=maxEntryHeight - 2)

    def display(self, clear=True):
        """
        Redraw every widget on the Form and the Form itself.

        Also a handy spot to update the form on field/widget changes.
        """
        if not self.regexPattern.value:
            # No search pattern has been entered yet.
            self.fullMatch.labelColor = 'DANGER'  # Make the labels red on black
            self.fullMatch.value = "No match!"
            self.groupMatches.labelColor = 'DANGER'
            self.groupMatches.value = ""
        else:
            # A search pattern has been entered.
            # Set regexOptions based on the checkboxes above, start with 0.
            regexFlags = regex.set_regex_flags(ignoreCase=self.ignoreCase.value,
                                               multiLine=self.multiLine.value,
                                               dotAll=self.dotAll.value)
            try:
                # Use the try/except block to handle invalid regex errors while compiling.
                compiledRegex = re.compile(r"{}".format(str(self.regexPattern.value)), regexFlags)
                matchedRegex = compiledRegex.search(str(self.searchString.value))
                if matchedRegex:
                    # The regex search was successful.
                    self.fullMatch.labelColor = 'SAFE'  # Make the labels green on black
                    self.fullMatch.value = '"{}"'.format(str(self.searchString.value)[matchedRegex.start():matchedRegex.end()])
                    self.groupMatches.value = regex.build_groups_string(matchedRegex.groups())
                    maxOutHeight = int(self.get_half_way() / 2) - 3
                    # Only make these scrollable (editable) if needed.
                    self.groupMatches.editable = False
                    self.fullMatch.editable = False
                    if len(self.fullMatch.value.splitlines()) > maxOutHeight:
                        # Make the full match scrollable if there are enough lines to warrant it.
                        self.fullMatch.editable = True
                    if self.groupMatches.value:
                        # There were group matches.
                        self.groupMatches.labelColor = 'SAFE'
                        if len(self.groupMatches.value.splitlines()) > maxOutHeight:
                            # Make the groups scrollable if there are enough to warrant it.
                            self.groupMatches.editable = True
                else:
                    # The regex search was not successful.
                    self.fullMatch.labelColor = 'DANGER'
                    self.fullMatch.value = "No match!"
                    self.groupMatches.labelColor = 'DANGER'
                    self.groupMatches.value = ""
            except re.sre_compile.error:
                # The regex search pattern was invalid.
                self.fullMatch.labelColor = 'DANGER'
                self.fullMatch.value = "Invalid regex!"
                self.groupMatches.labelColor = 'DANGER'
                self.groupMatches.value = ""
        super(MainForm, self).display()


class RegexTester(npyscreen.NPSAppManaged):
    """RegexTester application."""
    def onStart(self):
        """Method to run when starting the application."""
        self.addForm("MAIN", MainForm, name="Python Regex Tester")


if __name__ == "__main__":
    mainApp = RegexTester()
    mainApp.run()

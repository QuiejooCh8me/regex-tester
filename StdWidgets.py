"""Widgets to use in the main application."""

import npyscreen


class FixedMultiLineEdit(npyscreen.MultiLineEditableBoxed):
    """
    Fix some problems with the MultiLineEditableBoxed widget:

    1) Have the parent form update every time the widget is changed.
    """

    # Need to set the correct widget type inside the box.
    _contained_widget = npyscreen.MultiLineEdit

    def when_value_edited(self):
        """Method to run every time the value of this widget is changed."""
        # Update every widget in the parent form everytime the value of this widget is changed.
        self.parent.display()


class FixedRoundCheckBox(npyscreen.RoundCheckBox):
    """
    Fix some problems with the RoundCheckBox widget:

    1) Have the parent form update every time the widget is changed.
    """

    def whenToggled(self):
        """Method to run every time the value of this widget is changed."""
        # Update every widget in the parent form everytime the value of this widget is changed.
        self.parent.display()

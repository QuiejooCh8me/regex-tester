"""Forms to use in the main application."""

import sys
import weakref

import npyscreen


class ExitForm(npyscreen.fmForm.Form):
    """Similar to ActionForm but with the 'Cancel' button removed and an 'Exit' instead of 'OK' button."""
    EXIT_BUTTON_TEXT = 'Exit'

    def edit(self):
        """
        Set up the form layout and widgets.

        This is mostly copy/pasted from ActionForm with unwanted bits removed and formatting cleaned up.
        """
        # Add an 'Exit' button. Will remove later.
        tmp_rely, tmp_relx = self.nextrely, self.nextrelx

        my, mx = self.curses_pad.getmaxyx()
        exit_button_text = self.EXIT_BUTTON_TEXT
        my -= self.__class__.OK_BUTTON_BR_OFFSET[0]
        mx -= len(exit_button_text)+self.__class__.OK_BUTTON_BR_OFFSET[1]
        self.ok_button = self.add_widget(self.__class__.OKBUTTON_TYPE, name=exit_button_text, rely=my, relx=mx, use_max_space=True)
        ok_button_postion = len(self._widgets__) - 1
        # Done adding buttons

        self.editing = True
        if self.editw < 0:
            self.editw = 0
        if self.editw > len(self._widgets__) - 1:
            self.editw = len(self._widgets__) - 1
        if not self.preserve_selected_widget:
            self.editw = 0

        if not self._widgets__[self.editw].editable:
            self.find_next_editable()
        self.ok_button.update()

        self.display()

        while not self._widgets__[self.editw].editable:
            self.editw += 1
            if self.editw > len(self._widgets__) - 2:
                self.editing = False
                return False

        self.edit_return_value = None
        while self.editing:
            if not self.ALL_SHOWN:
                self.on_screen()
            try:
                self.while_editing(weakref.proxy(self._widgets__[self.editw]))
            except TypeError:
                self.while_editing()
            self._widgets__[self.editw].edit()
            self._widgets__[self.editw].display()

            self.handle_exiting_widgets(self._widgets__[self.editw].how_exited)

            if self.editw > len(self._widgets__) - 1:
                self.editw = len(self._widgets__) - 1
            if self.ok_button.value:
                self.editing = False

            if self.ok_button.value:
                self.ok_button.value = False
                self.edit_return_value = self.on_ok()

        self.ok_button.destroy()
        del self._widgets__[ok_button_postion]
        del self.ok_button
        self.nextrely, self.nextrelx = tmp_rely, tmp_relx
        self.display()
        self.editing = False

        return self.edit_return_value

    def on_ok(self):
        """Exit the application when the user clicks the 'Exit' button."""
        sys.exit(0)


class SplitExitForm(ExitForm, npyscreen.SplitForm):
    """Meld the SplitForm and ExitForm classes."""
    pass

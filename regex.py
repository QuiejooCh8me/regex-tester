"""Functions for handling the regex groups features."""

import re


def build_groups_string(regexGroups):
    """Return a formatted string for displaying in a MultiLineEdit widget."""
    formattedGroupMatches = []
    index = 0
    for match in regexGroups:
        formattedGroupMatches.append('(Group #{idx}) "{mtc}"'.format(idx=index, mtc=match))
        index += 1
    return "\n".join(formattedGroupMatches)


def set_regex_flags(ignoreCase=False, multiLine=False, dotAll=False):
    """Set the regex flags."""
    # Set regexFlags based on the checkboxes above, start with 0.
    regexFlags = 0
    if ignoreCase:
        regexFlags = regexFlags | re.I
    if multiLine:
        regexFlags = regexFlags | re.M
    if dotAll:
        regexFlags = regexFlags | re.S
    return regexFlags
